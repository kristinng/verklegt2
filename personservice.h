#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "person.h"
#include "personrepos.h"
#include <list>
#include <algorithm>

class personservice
{
public:
    personservice();
    void addperson(person p);
    void datainput();
    void showlist();
    void datainputstart();
    std::list<person> list();
    void clearlist();
    std::list<person> searchpick(string input, string searchstring);
    std::list<person> searchpick(string input, int year);
    static bool compare_name(const person &first, const person &second);
    static bool compare_gender(const person &first, const person &second);
    static bool compare_birth(const person &first, const person &second);
    static bool compare_death(const person &first, const person &second);
    static bool compare_namereverse(const person &first, const person &second);
    static bool compare_genderreverse(const person &first, const person &second);
    static bool compare_birthreverse(const person &first, const person &second);
    static bool compare_deathreverse(const person &first, const person &second);
    std::list<person> sort(string sortby);
private:
    personrepos personrep;
};

#endif // PERSONSERVICE_H
