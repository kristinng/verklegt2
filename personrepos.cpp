#include "personrepos.h"

personrepos::personrepos()
{
    personlist = std::list<person>();
}

void personrepos::addperson(person p){//this function simply takes in a person and pushes it into the list and then
    personlist.push_back(p);
    ofstream outfile("SavedList.txt");//we open a text file and we iterate the entire list to the text file.
    if(outfile.is_open()){//everytime we overwrite what was in the old file so the text file always contains the latest edition of our master list
        for(std::list<person>::iterator iter = personlist.begin();iter!=personlist.end();iter++){//
            outfile << iter->name << "\t" << iter->gender << "\t" << iter->birthyear << "\t" << iter->deathyear << "\n";
        }
        outfile.close();// one particular note about this function is that we have to write out to the text file with tabs inbetween
    }// and endline in the correct places so we can read from our textfile everytime we start the program
    //see the datainputstart function more more about that
}

void personrepos::datainput(){//this function opens a input text file that the user can put into the list at any time
    ifstream inputfile("inputtext.txt");//so we can offer the user a simple way to fill his list
    char temp;//it simply opens the text file and reads the name until the first tab character
    int check=0;//after that if the program confirms we added a name then it can assume we have a person being added
    personrepos per;//and is allowed to add the rest of the information.
    if(inputfile.is_open()){
        while(!inputfile.eof())
        {
            bool anotherperson=false;
            check=0;
            person p = person();
                do{
                       inputfile.get(temp);
                       if((temp != '\t')&(temp != '\n')){
                           p.name += temp;
                           anotherperson=true;
                       }
                       else{
                           check++;
                       }
                  }while(check==0);
              if(anotherperson==true){
                    inputfile >> p.gender >> p.birthyear >> p.deathyear;
                    addperson(p);
                    }
        }
    inputfile.close();
    }
}

bool personrepos::is_empty(ifstream& inputfile){//this function checks if a text file includes any information

    return inputfile.peek() == std::ifstream::traits_type::eof();//we need this function to check if there is any information in the database that we need to add when the program starts
}
void personrepos::datainputstart(){//this code is very similar to datainput. we are reading a text file with the same format both times
    personrepos per;//the biggest difference is basically that this function only does everything after checking if there is information in the file
    char temp;//because sometimes there might not be anything in the database text file and then we dont want to get stuck trying to add something from it
    ifstream inputfile("SavedList.txt");//for various reasons we could not just use this function for both datainputs from text file...
    if(inputfile.is_open()){
        if(per.is_empty(inputfile)==false){
            while(!inputfile.eof())
            {
                bool anotherperson=false;
                int check=0;
                person p = person();
                    do{
                           inputfile.get(temp);

                           if((temp != '\t')&(temp != '\n')){
                               p.name += temp;
                               anotherperson=true;
                           }
                           else{
                               check++;
                           }
                      }while(check==0);
                  if(anotherperson==true){
                        inputfile >> p.gender >> p.birthyear >> p.deathyear;
                        personlist.push_back(p);
                        }
            }
       }
    }
}
list<person> personrepos::list(){

    return personlist;
}
void personrepos::clearlist(){
    personlist.clear();//this clears all the elements in the list so we dont just clear the text file and then add the elements immediately again from the list
    ofstream outfile("SavedList.txt");// we just overwrite the text file to remove all of its contents.
        outfile.close();

}
list<person> personrepos::searchpick(string input,string searchstring){//search gets an input so it knows if its checking gender,name,birth or death  and a searchstring/year
    std::list<person> temp;//we iterate through our master list to see if we have a match and if we do we copy that person to a new list
    person p=person();//the new list then gets returned after we have iterated and added all our matches
    if(input == "gender"){
        for(std::list<person>::iterator iter = personlist.begin(); iter != personlist.end(); iter++){
            if(iter->gender == searchstring){
                p.name=iter->name;
                p.gender=iter->gender;
                p.birthyear=iter->birthyear;
                p.deathyear=iter->deathyear;
                temp.push_back(p);
            }
        }
    }
    else if(input == "name"){
        for(std::list<person>::iterator iter = personlist.begin(); iter != personlist.end(); iter++){
            if(iter->name == searchstring){
                p.name=iter->name;
                p.gender=iter->gender;
                p.birthyear=iter->birthyear;
                p.deathyear=iter->deathyear;
                temp.push_back(p);
            }
        }
    }

    return temp;
}
list<person> personrepos::searchpick(string input,int year){
    std::list<person> temp;

    person p=person();
    if(input == "birthyear"){
        for(std::list<person>::iterator iter = personlist.begin(); iter != personlist.end(); iter++){
            if(iter->birthyear == year){
                p.name=iter->name;
                p.gender=iter->gender;
                p.birthyear=iter->birthyear;
                p.deathyear=iter->deathyear;
                temp.push_back(p);
            }
        }
     }
     else if(input == "deathyear"){
        for(std::list<person>::iterator iter = personlist.begin(); iter != personlist.end(); iter++){
            if(iter->deathyear == year){
                p.name=iter->name;
                p.gender=iter->gender;
                p.birthyear=iter->birthyear;
                p.deathyear=iter->deathyear;
                temp.push_back(p);
            }
        }
     }

    return temp;
}

