#include "personservice.h"


personservice::personservice()
{
    personrep =personrepos();
}


void personservice::addperson(person p)
{
    personrep.addperson(p);
}
void personservice::datainput(){
    personrep.datainput();
}
void personservice::datainputstart(){
    personrep.datainputstart();
}
list<person> personservice::list(){
    return personrep.list();
}
void personservice::clearlist(){
    personrep.clearlist();
}
list<person> personservice::searchpick(string input,string searchstring){
    return personrep.searchpick(input, searchstring);
}
list<person> personservice::searchpick(string input,int year){
    return personrep.searchpick(input, year);
}

bool personservice::compare_name (const person& first, const person& second) {
    return first.name < second.name;
}
bool personservice::compare_gender (const person& first, const person& second) {
    return first.gender < second.gender;
}
bool personservice::compare_birth (const person& first, const person& second) {
    return first.birthyear < second.birthyear;
}

bool personservice::compare_death (const person& first, const person& second) {
    return first.deathyear < second.deathyear;
}
bool personservice::compare_namereverse (const person& first, const person& second) {
    return first.name > second.name;
}
bool personservice::compare_genderreverse (const person& first, const person& second) {
    return first.gender > second.gender;
}
bool personservice::compare_birthreverse (const person& first, const person& second) {
    return first.birthyear > second.birthyear;
}

bool personservice::compare_deathreverse (const person& first, const person& second) {
    return first.deathyear > second.deathyear;
}

list<person> personservice::sort(string sortby){
    std::list<person>plist;
    plist=list();
    if (sortby == "name"){
        plist.sort(compare_name);
    }
    else if (sortby == "gender"){
        plist.sort(compare_gender);
    }
    else if (sortby == "birth"){
        plist.sort(compare_birth);
    }
    else if (sortby == "death"){
        plist.sort(compare_death);
    }
    else if (sortby == "rname"){
        plist.sort(compare_namereverse);
    }
    else if (sortby == "rgender"){
        plist.sort(compare_genderreverse);
    }
    else if (sortby == "rbirth"){
        plist.sort(compare_birthreverse);
    }
    else if (sortby == "rdeath"){
        plist.sort(compare_deathreverse);
    }
    return plist;
}

