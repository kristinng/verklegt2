#-------------------------------------------------
#
# Project created by QtCreator 2014-11-30T11:10:16
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = verklegtverkefni
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    personservice.cpp \
    person.cpp \
    personrepos.cpp

HEADERS += \
    consoleui.h \
    personservice.h \
    person.h \
    personrepos.h
