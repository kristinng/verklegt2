#ifndef CONSOLEUI_H
#define CONSOLEUI_H
#include <iostream>
#include <string>
#include "personservice.h"

class ConsoleUI
{
public:
    ConsoleUI();
    void start();
    void showlist(list<person> plist);
    void searchlist();
    void sortoptions();

private:
    personservice Personservice;
};

#endif // CONSOLEUI_H
