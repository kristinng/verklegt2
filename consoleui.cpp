#include "consoleui.h"

using namespace std;

ConsoleUI::ConsoleUI()
{
    Personservice = personservice();
}

void ConsoleUI::start()
{
    Personservice.datainputstart();
    string input;
    bool exit=false;
    cout << "Welcome!\nThis program can make a list of computer scientists and sort them as you want.\n";
    while(exit==false){
        cout << "These are all possible commands:\n";
        cout << "\"add\" - This command adds people to the list.\n";
        cout << "\"show\" - This command displays the list on the screen.\n";
        cout << "\"search\" - This command searches the list.\n";
        cout << "\"delete\" - This command clears the list.\n";
        cout << "\"exit\" - This command exits the program.\n";
        cin >> input;
        if(input == "add"){
            cout << "Type \"m\" for manual input or \"t\" for text file input:\n";
                cin >> input;
                if(input =="t"){
                    Personservice.datainput();
                    cout << "Data from text file has been added to the list.\n\n";
                }
                else if(input == "m"){
                    string again;
                    bool exit=false;
                    while(exit==false){
                        person p = person();
                        cout << "Input a name: ";
                        cin.ignore();
                        getline(cin, p.name);
                        cout << "Input gender: ";
                        cin >> p.gender;
                        cout << "Input year of birth: ";
                        cin >> p.birthyear;
                        cout << "Input year of death: ";
                        cin >> p.deathyear;
                        Personservice.addperson(p);
                        cout << "Person successfully added!\n";
                        cout << "Type \"yes\" if you would like to add another person, anything else exits.\n";
                        cin >> again;
                        if(again !="yes"){
                            exit=true;
                        }
                    }
                }
            }

        else if(input == "show"){//calls a function that offers the user the ability to sort
            sortoptions();
        }
        else if(input == "search"){
            string again;
            bool exit=false;
            while(exit==false){
                searchlist();
                cout << "Type \"yes\" if you would like to add search again, anything else exits.\n";
                cin >> again;
                if(again !="yes"){
                    exit=true;
                }
            }
        }
        else if(input == "delete"){
                Personservice.clearlist();
                cout << "List has been deleted.\n\n";
                }
        else if(input == "exit"){
            exit = true;
        }
        else{
            cout << "That is not a valid command.\n\n";
        }

    }
}

void ConsoleUI::showlist(list<person>plist){//this function receives a list and iterates through it to display all its elemts
    cout << "_____________________________________\n\n";
    for(list<person>::iterator iter = plist.begin(); iter != plist.end(); iter++){
        cout << iter->name << "\t" << iter->gender << "\t" << iter->birthyear << "\t" << iter->deathyear<< "\n";
    }
    cout << "_____________________________________\n\n";
}
void ConsoleUI::searchlist(){// the way this function works is it takes in a input+ a searchstring
    string input,searchstring;//the they both get sent as parameters to the repository layer and there we iterate through the master list
    int year;//and if we find a match in the master list we push that person into a new list and repeat until we have gone through the master list
    list<person>resultslist;//after that we return the list to the showlist function and it displays it
    cout << "These are all possible search options: \n";
    cout << "Type \"name\" - to search by name.\n";
    cout << "Type \"gender\" - to search by gender.\n";
    cout << "Type \"birthyear\" - to search by birthyear.\n";
    cout << "Type \"deathyear\" - to search by deathyear.\n";
    cin >> input;
    cin.ignore();
    if(input == "name"){
        cout << "Input a name to search for: ";
        getline (cin,searchstring);
        resultslist = Personservice.searchpick(input, searchstring);
        if(0<resultslist.size()){
            cout << resultslist.size()<<" results found: \n";
        showlist(resultslist);
        }
        else{
            cout << "No results found.\n\n";
        }
    }
    else if(input == "gender"){
        cout << "Input gender you would like to search for: ";
        cin >> searchstring;
        resultslist=Personservice.searchpick(input, searchstring);
        if(0<resultslist.size()){//here we check if the list has any elements, if it does not we have not found any matches
            cout << resultslist.size()<<" results found: \n";
            showlist(resultslist);
        }
        else{
            cout << "No results found.\n\n";
        }

    }
    else if(input == "birthyear"){
        cout << "Input a year to search for: ";
        cin >> year;
        resultslist=Personservice.searchpick(input, year);
        if(0<resultslist.size()){
            cout << resultslist.size()<<" results found: \n";
            showlist(resultslist);
        }
        else{
            cout << "No results found.\n\n";
        }
    }
    else if(input == "deathyear"){
        cout << "Input a year to search for: ";
        cin >> year;
        resultslist=Personservice.searchpick(input, year);
        if(0<resultslist.size()){
            cout << resultslist.size()<<" results found: \n";
            showlist(resultslist);
        }
        else{
            cout << "No results found.\n\n";
        }
    }
    else{
        cout << "That is not a valid option\n\n";
    }

}
void ConsoleUI::sortoptions(){// the sort function calls to the service layer where we sort the list by what the user chose to input
    bool exit=false;
    string sortby, again;
    while(exit==false){
        cout << "Type \"original\" to see the list as it is.\n";
        cout << "List can be sorted by \"name\" , \"gender\" , \"birth\" or \"death\".\n";
        cout << "Type an \"r\" in front of any of the sort options available to see them reversed.\n";
        cout << "Type \"exit\" to return to the main menu.\n";
        cin >> sortby;
        if(sortby == "original"){
            list<person>plist;
            plist=Personservice.list();
            showlist(plist);
            cout << "If you would like to try a different sort type \"again\" ,anything else exits\n";
            cin >> again;
            if(again != "again"){
                exit=true;
            }
        }
        else if(sortby != "exit"){
            showlist(Personservice.sort(sortby));
            cout << "If you would like to try a different sort type \"again\" ,anything else exits\n";
            cin >> again;
            if(again != "again"){
                exit=true;
            }
        }
        else if(sortby == "exit"){
            exit=true;
        }
        else{
            cout << "Invalid input, try again.\n";
        }

    }
}
