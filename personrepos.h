#ifndef PERSONREPOS_H
#define PERSONREPOS_H

#include <list>
#include "person.h"
#include <fstream>
#include <iostream>

class personrepos
{
public:
    personrepos();
    void addperson(person p);
    void datainput();
    void showlist();
    void datainputstart();
    bool is_empty(ifstream &inputfile);
    std::list<person> list();
    void clearlist();

    std::list<person> searchpick(string input, string searchstring);
    std::list<person> searchpick(string input, int year);
private:
    std::list<person> personlist;
};

#endif // PERSONREPOS_H



